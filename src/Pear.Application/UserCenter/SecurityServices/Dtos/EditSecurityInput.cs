﻿namespace Pear.Application.UserCenter
{
    /// <summary>
    /// 编辑权限信息
    /// </summary>
    public class EditSecurityInput
    {
        /// <summary>
        /// 菜单名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 菜单图标
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 菜单地址
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 菜单打开方式
        /// </summary>
        public string Target { get; set; }
        /// <summary>
        /// 菜单类型
        /// </summary>
        public int? Type { get; set; }
        /// <summary>
        /// 权限标识
        /// </summary>
        public string Authorize { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 序号/排序
        /// </summary>
        public int Sequence { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// 上级菜单Id
        /// </summary>
        public int? ParentId { get; set; }
    }
}